<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<title>Form validation Username, Email, dan Phone Number</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>
<body>
	<form method="post" action="index.php" onsubmit="return validateForm(), e()">
		<p>Username:
			<input type="text" size="32" name="inputfield">
		</p>
		<p>Email:
			<input type="email" size="32" name="inputfield">
		</p>
		<p>Phone Number: 
			<input type="text" size="14" name="inputphone">
		</p>
		<p><input type="submit"></p>
	</form>
	<script type="text/javascript">
			$('input[name=inputfield]').keyup(function(validateForm){
				if($(this).val()!=''){
					$(this).val( $(this).val().toLowerCase());
					//alert("Nama harus diisi menggunakan huruf kecil saja!")
					return false;
				}
			});
			$('input[name=inputphone]').keydown(function (e){
				if($.inArray(e.keyCode, [8,9,27,13,110,57,48,189,32,187]) !== -1 || 
					// tinggal : -=()
					(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
					
					(e.keyCode >= 35 && e.keyCode <=40)){
						return;
					}
					if((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)){
						e.preventDefault();
					}
					
			});
	</script>
</body>
</html>



