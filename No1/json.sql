-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2018 at 03:44 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `json`
--

-- --------------------------------------------------------

--
-- Table structure for table `json`
--

CREATE TABLE IF NOT EXISTS `json` (
  `name` text NOT NULL,
  `address` text NOT NULL,
  `hobbies` varchar(255) NOT NULL,
  `is_married` int(11) NOT NULL,
  `university` varchar(255) NOT NULL,
  `skills` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `json`
--

INSERT INTO `json` (`name`, `address`, `hobbies`, `is_married`, `university`, `skills`) VALUES
('rani triani gustia', 'Jln. Sukamenak Gg. Sukamanah II RT.01/16', 'Ngoding', 0, 'SMKN 1 Katapang', 'make program');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `json`
--
ALTER TABLE `json`
  ADD PRIMARY KEY (`university`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
